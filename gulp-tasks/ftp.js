module.exports = function (gulp, plugins, path) {
	return () => {

		function getConn() {
			return plugins.ftp.create({
				host: 'eclisse31337.artbayard.ru',
				user: 'eclisse.artbayard.ru|ftp_eclisse',
				pass: '1234'
			});
		}

		var conn = getConn();

		return gulp.src(path.public.all)
			.pipe(conn.dest('./'));
	}
}