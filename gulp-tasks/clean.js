module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src(path.public.root, {
				read: false,
				allowEmpty: true
			})
			.pipe(plugins.plumber({
				errorHandler: plugins.notify.onError()
			}))
			.pipe(plugins.clean());
	}
}