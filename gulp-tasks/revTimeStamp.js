module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src(path.public.htmlAll)
			.pipe(plugins.plumber({
				errorHandler: plugins.notify.onError()
			}))
			.pipe(plugins.revts({
				'replaces': [
					[/{VERSION_REPlACE}/g, '%DT%']
				],
			}))
			.pipe(gulp.dest(path.public.html))
			.pipe(plugins.connect.reload());
	}
}