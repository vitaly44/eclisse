//слайдер
$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        loop: true,
        dots: true,
        nav: true,
        margin: 0,
        items: 1,
        navText: ['<img src="/img/slider-arrow-left.svg" />', '<img src="/img/slider-arrow-right.svg" />'],
        navContainer: '.owl-navs .nav',
        dotsContainer: '.owl-navs .dots',
    });
});

//отображение|скрытие мобильного меню по нажатию на "бургер"
$(document).ready(function () {
    var $hamburger = $('.hamburger');
    var _body = $('body');

    $hamburger.on('click', function (e) {
        $(this).toggleClass('is-active');

        $('.mob').toggleClass('show');
        $('nav .col').removeClass('is_submenu');
        $('nav .col .submenu').removeClass('show');

        if (_body.hasClass('is_submenu')) {
            _body.removeClass('is_submenu');
        }
    });
});


//скрытие подменю
function hide_submenu() {
    $('nav .submenu').removeClass('show');
    $('nav .col').removeClass('is_submenu');
}

//скрытие подменю по нажатию на любую область вне "шапки" сайта
$(document).ready(function () {
    $('html, body').on('click', function () {
        var _body = $('body');
        var eventInMenu = $(event.target).parents('header');

        if (!eventInMenu.length) {
            hide_submenu();
            $('header .mob').removeClass('show');
            _body.removeClass('is_submenu');
            $(".hamburger").removeClass("is-active");
        }
    })
});

//отображение|скрытие подменю по нажатию на стрелочку в "шапке" сайта
$(document).ready(function () {
    $('nav .more').on('click', function (e) {
        var _body = $('body');
        var _this = $(this);
        var _parent = _this.parent();
        var _parent_isShow = _parent.hasClass('is_submenu');
        var _submenu = _parent.find('.submenu');
        var _submenu_isShow = _submenu.hasClass('show');

        $('nav .col').removeClass('is_submenu');
        $('nav .submenu').removeClass('show');
        _body.removeClass('is_submenu');

        if (_parent_isShow && _submenu_isShow) {
            _parent.removeClass('is_submenu');
            _body.removeClass('is_submenu')
            _submenu.removeClass('show');
        } else {
            _body.addClass('is_submenu');
            _parent.addClass('is_submenu');
            _submenu.addClass('show');
        };
    });
});

//отображение|скрытие разделов и их адаптивность, где есть отображение по нажатию, в блоках "привелегии, преимущества"
$(document).ready(function () {
    var _toggle = $('[data-toggle]');

    _toggle.click(function () {
        var _this = $(this);
        var _target = $(_this.data('toggle'));
        var _targetMob = _this.find('.list__item-mob');
        var _isTargetActive = _target.hasClass('active');
        var _root = _this.parent('.list__item');
        var _childs = _root.find('.list__item-item');
        var _targetMobs = _root.find('.list__item-mob');

        _targetMobs.removeClass('active');
        _childs.removeClass('active');
        _childs.find('.toggle_target').removeClass('active');

        if (_isTargetActive) {
            _targetMob.removeClass('active');
            _this.removeClass('active');
            _target.removeClass('active');
        } else {
            var _parentOffsetLeft = -((_this.position().left - _this.width() > 0) ? _this.width() + 15 * 3 + 1 : 15);

            _targetMob.addClass('active').css({
                width: _root.width(),
                left: _parentOffsetLeft
            });
            _this.addClass('active');
            _target.addClass('active');


        }

        $(window).resize(function () {
            var _parent = _targetMob.parent();
            var _root = _parent.parent();

            var _parentOffsetLeft = -((_parent.position().left - _parent.width() > 0) ? _parent.width() + 15 * 3 + 1 : 15);

            _targetMob.css({
                width: _root.width(),
                left: _parentOffsetLeft
            });
        })
    })
});

//фиксация меню при скроле
$(window).scroll(function () {
    var header = $('header'),
        scroll = $(window).scrollTop(),
        top_shift = header.outerHeight(),
        _isSubMenu = $('.is_submenu').length;


    if (scroll >= top_shift) {
        if (!header.hasClass('fixed')) {

            header.addClass('fixed');
        }
    } else {
        header.removeClass('fixed');
    }

    if ($('header .mob').hasClass('show') && !_isSubMenu) {
        $('header .mob').removeClass('show');
        $(".hamburger").removeClass("is-active");
    }
});

//скрытие подменю при скроле
$(window).scroll(function () {
    var _isSubMenuClose = false;
    var _subMenu = $('.is_submenu');
    var _scrollTop = $('.is_submenu');

    _isSubMenuClose = _subMenu.length;

    if (window.innerWidth > 1025 || !_isSubMenuClose) {
        hide_submenu();
    }
});

//валидация форм
$(document).ready(function () {
    $('form').validator()
        .on('submit', function (e) {

            var _this = $(this);

            if (e.isDefaultPrevented()) {

                _this.addClass('was-validate');

                console.log('invalid');

            } else {

                _this.removeClass('was-validate');

                e.preventDefault();
            }
        })
        .on('invalid.bs.validator', function (e) {
            $(e.relatedTarget).parents('.input_field').addClass('has-validate');
        })
        .on('valid.bs.validator', function (e) {
            $(e.relatedTarget).parents('.input_field').removeClass('has-validate');
        })
});

//скроллбар для длинных блоков ограниченных по высоте

//валидация форм
$(document).ready(function () {
    $('.scrollbar-inner').scrollbar();
});